#  ¿Qué es un pantron de diseños?  #


Los patrones del diseño tratan los problemas del diseño que se repiten y que se presentan en situaciones particulares del diseño, con el fin de proponer soluciones a ellas. Por lo tanto, los patrones de diseño son soluciones exitosas a problemas comunes

## Los tipo de patrones que existen se clasifican según:

Patrones de construcción 

- Abstract Factory
- Builder
- Factory Method
- Prototype
- Singleton

Patrones de estructuración
 
- Adapter
- Bridge
- Composite
- Decorator
- Facade
- Flyweight
- Proxy


Patrones de comportamiento

- Chain of Responsability (El patrón de diseño Cadena de responsabilidad)
- Command
- Interpreter
-  Iterator
- Mediator
- Memento (o Snapshot) 
- Observer 
- State
- Strategy
- Template Method 
- Visitor

![Con titulo](Imagen/Imagen3.png "titulo")
![Con titulo](Imagen/Imagen4.png "titulo")






![Con titulo](Imagen/Imagen2.png "titulo")

 **Patrones de creación.**Estos patrones se utilizan cuando debemos crear objetos pero debemos tomar decisiones dinámicamente en el proceso de creación. Para ésto lo que hacemos es abstraer el proceso de creación de los objetos para realizar la decisión de qué objetos crear o cómo crearlos para el momento en que se tenga que hacer. 

Patron Singleton: Es un patrón diseñado para limitar la creación de objetos pertenecientes a una clase. El objetivo de este patrón es el de garantizar que una clase solo tenga una instancia (o ejemplar) y proporcionar un punto de acceso global a ella. Esta patrón; por ejemplo, suele ser utilizado para las conexiones a bases de datos.

Este patrón se implementa haciendo privado el constructor de la clase y creando (en la propia clase) un método que crea una instancia del objeto si este no existe.


`public class ClaseSingleton {
    private static ClaseSingleton instanciaUnica = new ClaseSingleton();
 
    private ClaseSingleton() {}
 
    public static ClaseSingleton getInstance() {
        return instanciaUnica;
    }
}` MODIFICAAAAARR EL CODIGO

El código habla por si solo pero vamos a explicarlo línea por línea:

- El primer paso es crear en la propia clase un objeto de la propia clase, que será la única instancia de nuestra clase.
- Para impedir que se puedan crear nuevas instancias de nuestra clase tenemos que declarar el constructor de nuestra clase como privado para evitar que se pueden crear objetos desde fuera desde nuestra clase.
- Finalmente el ultimo paso es declarar un método publico para obtener el objeto que hemos creado en un primer momento y de esta forma dotar de una forma de acceso a la única instancia de nuestra clase.

Patron Prototype: Este patrón prototype tiene un objetivo muy sencillo: crear a partir de un modelo. Permite crear objetos prediseñados sin conocer detalles de cómo crearlos. Esto lo logra especificando los prototipos de objetos a crear. Los nuevos objetos que se crearán de
los prototipos, en realidad, son clonados. Tiene como finalidad crear nuevos
objetos duplicándolos, clonando una instancia creada previamente.

Cuando utilizar este patrón:

Aplica en un escenario donde sea necesario la creación de objetos parametrizados
como "recién salidos de fábrica" ya listos para utilizarse, con la gran ventaja de la
mejora de la performance: clonar objetos es más rápido que crearlos y luego setear
cada valor en particular.

Este patrón debe ser utilizado cuando un sistema posea objetos con datos repetitivos,
en cuanto a sus atributos: por ejemplo, si una biblioteca posee una gran cantidad de
libros de una misma editorial, mismo idioma, etc. Hay que pensar en este patrón
como si fuese un fábrica que tiene ciertas plantillas de ejemplos de sus prodcutos y, a
partir de estos prototipos, puede crear una gran cantidad de productos con esas
características.

 **PATRON PROTOTYPE**

![Con titulo](Imagen/Imagen7.png "titulo")




- Prototype: declara la interface del objeto que se clona. Suele ser una clase abstracta.
- PrototypeConcreto: las clases en este papel implementan una operación por medio de la clonación de sí mismo.
- Cliente: crea nuevos objetos pidiendo al prototipo que se clone.

Los objetos de Prototipo Concreto heredan de Prototype y de esta forma el patrón se
asegura de que los objetos prototipo proporcionan un conjunto consistente de
métodos para que los objetos clientes los utilicen.

**Patrones de comportamiento.**Fundamentalmente especifican el comportamiento entre objetos de nuestro programa. 

Patron Iterator: Este patron podemos utilizarlo cuando necesitemos recorrer secuencialmente los objetos de un elemento agregado sin exponer su representación interna.

Así pues, este patrón de diseño nos resultará útil para acceder a los elementos de un array o colección de objetos contenida en otro objeto:


**PARTON ITERATOR**
![Con titulo](Imagen/Imagen6.png "titulo")

 **Patrones estructurales.**Nos describen como utilizar estructuras de datos complejas a partir de elementos más simples. Sirven para crear las interconexiones entre los distintos objetos y que estas relaciones no se vean afectadas por cambios en los requisitos del programa. 


Patron Adapter: Este patrón permite que trabajen juntas clases con interfaces incompatibles.

Para ello, un objeto adaptador reenvía al otro objeto los datos que recibe (a través de los métodos que implementa, definidos en una clase abstracta o interface) tras manipularlos en caso necesario.


**PATRON ADAPTER**

![Con titulo](Imagen/Imagen5.png "titulo")
#  ¿Qué son los antipatrones?  #
Un antipatrón de diseño es un patrón de diseño que invariablemente conduce a una mala solución para un problema.

--------------------------------------------------------------

Por ejemplo, en la programación orientada a objetos, la idea es separar el software en partes pequeñas llamadas objetos. Un antipatrón en la programación orientada a objetos es un God object que realiza muchas funciones que se separarían mejor en diferentes objetos.

Por ejemplo:


     class GodObject {
    function PerformInitialization() {}
    function ReadFromFile() {}
    function WriteToFile() {}
    function DisplayToScreen() {}
    function PerformCalculation() {}
    function ValidateInput() {}
    // and so on... //
    }

El ejemplo anterior tiene un objeto que hace todo. En la programación orientada a objetos, sería preferible tener responsabilidades bien definidas para los diferentes objetos para mantener el código menos acoplado y, en última instancia, más fácil de mantener:


    class FileInputOutput {
    function ReadFromFile() {}
    function WriteToFile() {}
     }

    class UserInputOutput {
    function DisplayToScreen() {}
    function ValidateInput() {}
    }

    class Logic {
    function PerformInitialization() {}
    function PerformCalculation() {}
    }

La conclusión es que hay buenas maneras de desarrollar software y patrones de uso común (design patterns), pero también hay formas en que se desarrolla e implementa un software que puede llevar a problemas. Los patrones que se consideran malas prácticas de desarrollo de software son anti-patrones.

